/*
Author: Daniel Hayward
Desc: Implements the class gVector4 by overloading basic operators and some functions
*/
#include "gVector4.h"
#include <iostream>

gVector4::gVector4() {
	for (int i = 0; i < 4; i++) {
		data[i] = 0.0f;
	}
}

gVector4::gVector4(float x, float y, float z, float w) {
	data[0] = x;
	data[1] = y;
	data[2] = z;
	data[3] = w;
}

/// Returns the value at index
float gVector4::operator[](unsigned int index) const {
	if (index >= 4) {
		index = 1;
	}
	return data[index];
}

/// Return a reference to the value at index
float& gVector4::operator[](unsigned int index) {
	if (index >= 4) {
		index = 1;
	}
	return data[index];
}

/// Prints the vector to standard output in a nice format
void gVector4::print() {
	std::cout << "<";
	for (int i = 0; i < 4; i++) {
		std::cout << data[i];
		if (i != 3) {
			std::cout << ", ";
		}
	}
	std::cout << ">" << std::endl;
}

/// Returns the geometric length of the vector
float gVector4::length() const {
float sum = 0.0f;
for (int i = 0; i < 3; i++) {
	sum += data[i] * data[i];
}
return sqrt(sum);
}

/// Checks if v1 == v2
bool gVector4::operator==(const gVector4& v2) {
	for (int i = 0; i < 4; i++) {
		if (abs(data[i] - v2[i]) > 0.0001f) {
			return false;
		}
	}
	return true;
}

/// Checks if v1 != v2
bool gVector4::operator!=(const gVector4& v2) {
	if (!(*this == v2)) {
		return true;
	}
	else {
		return false;
	}
}

/// Vector Addition (v1 + v2)
gVector4 gVector4::operator+(const gVector4& v2) {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++) {
		temp[i] = data[i] + v2[i];
	}
	return temp;
}

/// Vector Subtraction (v1 - v2)
gVector4 gVector4::operator-(const gVector4& v2) {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++) {
		temp[i] = data[i] - v2[i];
	}
	return temp;
}

/// Scalar Multiplication (v * c)
gVector4 gVector4::operator*(float c) {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++) {
		temp[i] = c * data[i];
	}
	return temp;
}

/// Scalar Multiplication (c * v)
gVector4 operator*(float c, const gVector4& v) {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++) {
		temp[i] = c * v[i];
	}
	return temp;
}

/// Scalar Division (v / c)
gVector4 gVector4::operator/(float c) {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++) {
		temp[i] = data[i] / c;
	}
	return temp;
}

/// Dot Product (v1 * v2)
float gVector4::operator*(const gVector4& v2) {
	float sum = 0.0f;
	for (int i = 0; i < 4; i++) {
		sum += data[i] * v2[i];
	}
	return sum;
}

/// Cross Product (v1 % v2)
/// note that for our purposes, the cross product is performed with the first three components of each vector and the result's w-coordinate is set to 0
gVector4 gVector4::operator%(const gVector4& v2) {
	gVector4 cross = gVector4();

	cross[0] = data[1] * v2[2] - data[2] * v2[1];
	cross[1] = data[2] * v2[0] - data[0] * v2[2];
	cross[2] = data[0] * v2[1] - data[1] * v2[0];
	cross[3] = 0;

	return cross;
}

/// Component-wise Product (v1 ^ v2) -- note, because of operator precedence, always use parentheses with this operator
/// example: v3 = c * (v1 ^ v2)
gVector4 gVector4::operator^(const gVector4& v2) {
	gVector4 power = gVector4();

	for (int i = 0; i < 4; i++){
		power[i] = pow(data[i], v2[i]);
	}

	return power;
}
