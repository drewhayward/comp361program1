//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania

//modified January 25, 2017 at Grove City College

/*
Author: Daniel Hayward
Desc: Implements tests for the gVector4 and gMatrix4 class.
*/

#include "gVector4.h"
#include "gMatrix4.h"
#include <assert.h>
#include <iostream>

int main(){
	gVector4 f(1.0f, 0.0f, 1.0f, 0.0f), g(0.0f, 1.0f, 0.0f, 0.0f), h(0.0f, 0.0f, 1.0f, 0.0f); //builds 3 vectors: f, g, and h
	gVector4 z; //create vector z with the default constructor
	gMatrix4 a(f,g,h,z); //builds a 4x4 matrix with the three vectors, f, g, h, and z
	gMatrix4 b; //builds a 4x4 matrix with the default constructor

	/* Testing the gVector4 */
	gVector4 test1(1.0f, 1.0f, 1.0f, 1.0f);
	gVector4 test2(2.0f, 2.0f, 2.0f, 2.0f);
	
	// Testing [] operator
	for (int i = 0; i < 4; i++) {
		assert(test1[i] == 1.0f);
	}
	
	// Testing length function
	assert(abs(test1.length() - 1.73205080f) < 0.0001f);

	// Testing [] assignment operator
	for (int i = 0; i < 4; i++) {
		test1[i] = 2.0f;
		assert(test1[i] == 2.0f);
		test1[i] = 1.0f;
	}

	// Testing the various operators
	gVector4 test3 = test1;
	test3 = test3 * 2;
	assert(test2 == test3);
	test3 = test3 / 2;
	assert(test1 == test3);
	assert(test3 != test2);
	test3 = test3 + test3;
	assert(test3 == test2);
	test3 = test3 - test1;
	assert(test3 == test1);

	test1 = gVector4(1.0f, 2.0f, 0.0f, 3.0f);
	test2 = gVector4(2.0f, 1.0f, 10.0f, 2.0f);
	assert(test1 * test2 == 10.0f);


	a.print(); //print matrix a
	/* 1.0 0.0 1.0 0.0
	   0.0 1.0 0.0 0.0
	   0.0 0.0 1.0 0.0
	   0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	a[0][0] = 3.2; //set the first row and column to 3.2
	a[1][2] = 1.6; //set the second row and first column to 1.6
	a.print(); //print matrix a
	/* 3.2 0.0 1.0 0.0
	   0.0 1.0 1.6 0.0
	   0.0 0.0 1.0 0.0 
	   0.0 0.0 0.0 0.0 */

	f.print(); //print vector f
	/* 1.0 0.0 1.0 0.0 */
	f[0] = 1.2; //change the x value of f to 1.2
	f.print(); //print the updated vector f
	/* 1.2 0.0 1.0 0.0 */
	z = f + g + h; //set z to the sum of f, g, and h
	z.print(); // print the updated vector z
	/* 1.2 1.0 2.0 0.0 */

	b = gMatrix4::identity(); //set b to the identity matrix

	b.print(); //verify that b is set correctly
	/* 1.0 0.0 0.0 0.0
	   0.0 1.0 0.0 0.0
	   0.0 0.0 1.0 0.0
	   0.0 0.0 0.0 1.0 */
	gMatrix4 zeros = gMatrix4();
	assert(b - b == zeros);
	assert(b + b == b * 2);
	assert(b == b.transpose());

	// Testing transformation matricies
	f = gVector4(1.0f, 1.0f, 1.0f, 0.0f);
	g = gMatrix4::rotationX(90) * f;
	h = gVector4(1.0f, -1.0f, 1.0f, 0.0f);
	assert(g == h);
	g = gMatrix4::rotationY(90) * f;
	h = gVector4(1.0f, 1.0f, -1.0f, 0.0f);
	assert(g == h);
	g = gMatrix4::rotationZ(90) * f;
	h = gVector4(-1.0f, 1.0f, 1.0f, 0.0f);
	assert(g == h);
	f = gVector4(0.0f, 0.0f, 0.0f, 1.0f);
	g = gMatrix4::translation3D(1.0f, 1.0f, 1.0f) * f;
	h = gVector4(1.0f, 1.0f, 1.0f, 1.0f);
	assert(g == h);
	f = gVector4(1.0f, 1.0f, 1.0f, 0.0f);
	g = gMatrix4::scale3D(10.0f, 10.0f, 10.0f) * f;
	h = gVector4(10.0f, 10.0f, 10.0f, 0.0f);
	assert(g == h);



	

	// Testing matrix multiplication
	gVector4 e = gVector4(86.0f, 47.0f, -52.0f, -19.0f);
	f = gVector4(-6.0f, -8.0f, 75.0f, 43.0f);
	g = gVector4(100.0f, -100.0f, 47, 82.0f);
	h = gVector4(-86.0f, -96.0f, 76.0f, -55.0f);
	gMatrix4 matrixA(e, f, g, h);

	e = gVector4(-36.0f, -17.0f, -69.0f, -2.0f);
	f = gVector4(-94.0f, -14.0f, -30.0f, 83.0f);
	g = gVector4(-33.0f, -12.0f, 49.0f, 65.0f);
	h = gVector4(-6.0f, -57.0f, 32.0f, -10.0f);
	gMatrix4 matrixB(e, f, g, h);

	e = gVector4(-5684.0f, -413.0f, -10500.0f, 539.0f);
	f = gVector4(-1765.0f, -3137.0f, 5705.0f, 3793.0f);
	g = gVector4(3757.0f, -5538.0f, 1027.0f, -6265.0f);
	h = gVector4(9942.0f, 5029.0f, 10778.0f, -2306.0f);
	gMatrix4 answer(e, f, g, h);

	assert(matrixA*matrixB == answer);

	// Testing matrix vector multiplication
	e = gVector4(-13.0f, -22.0f, -18.0f, 6.0f);
	f = gVector4(25.0f, 12.0f, -34.0f, 23.0f);
	g = gVector4(16.0f, -37.0f, 25, 23.0f);
	h = gVector4(42.0f, -39.0f, 27.0f, 48.0f);
	matrixA = gMatrix4(e, f, g, h);

	gVector4 vec = gVector4(-5, -43, 15, 16);

	gVector4 answerVec = gVector4(837.0f, -783.0f, 2254.0f, 2640.0f);
	assert(matrixA*vec == answerVec);




}