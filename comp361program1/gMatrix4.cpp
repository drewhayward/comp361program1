/*
Author: Daniel Hayward
Desc: Implements the class gMatrix4 by overloading basic operators and some functions.
	Also includes static functions to generate transformation matricies
*/
#include "gMatrix4.h"
#include "gVector4.h"

#include <iostream>

#define DEG_TO_RAD 3.141592653589793238463f/180.0f

/// Default Constructor.  Initialize to matrix of all 0s.
gMatrix4::gMatrix4() {
	for (int i = 0; i < 4; i++)
	{
		data[i] = gVector4();
	}
}

/// Initializes matrix with each vector representing a row in the matrix
gMatrix4::gMatrix4(const gVector4& row0, const gVector4& row1, const gVector4& row2, const gVector4& row3) {
	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}


/// Returns the values of the row at the index
gVector4 gMatrix4::operator[](unsigned int index) const {
	if (index >= 4) {
		index = 1;
	}
	return data[index];
}

/// Returns the values of the row at the index
gVector4& gMatrix4::operator[](unsigned int index) {
	if (index >= 4) {
		index = 1;
	}
	return data[index];
}

/// Returns a column of the matrix
gVector4 gMatrix4::getColumn(unsigned int index) const {
	gVector4 temp = gVector4();
	for (int i = 0; i < 4; i++)
	{
		temp[i] = data[i][index];
	}
	return temp;
}

/// Prints the matrix to standard output in a nice format
void gMatrix4::print() {
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			std::cout << data[i][j] << "\t";
		}
		std::cout << std::endl;
	}
}

/// Returns the transpose of the matrix (v_ij == v_ji)
gMatrix4 gMatrix4::transpose() const {
	gMatrix4 trans;
	for (int i = 0; i < 4; i++)
	{
		gVector4 row = getColumn(i);
		trans[i] = row;
	}
	return trans;
}

/// Creates a 3-D rotation matrix.
/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the x-axis
gMatrix4 gMatrix4::rotationX(float angle) {
	gVector4 
		e(1.0f, 0.0f, 0.0f, 0.0f), 
		f(0.0f, cos(DEG_TO_RAD * angle), -sin(DEG_TO_RAD * angle), 0.0f), 
		g(0.0f, sin(DEG_TO_RAD * angle), cos(DEG_TO_RAD * angle), 0.0f), 
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the y-axis
gMatrix4 gMatrix4::rotationY(float angle) {
	gVector4
		e(cos(DEG_TO_RAD * angle), 0.0f, sin(DEG_TO_RAD * angle), 0.0f),
		f(0.0f, 1.0f, 0.0f, 0.0f),
		g(-sin(DEG_TO_RAD * angle), 0.0f, cos(DEG_TO_RAD * angle), 0.0f),
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the z-axis
gMatrix4 gMatrix4::rotationZ(float angle) {
	gVector4
		e(cos(DEG_TO_RAD * angle), -sin(DEG_TO_RAD * angle), 0.0f, 0.0f),
		f(sin(DEG_TO_RAD * angle), cos(DEG_TO_RAD * angle), 0.0f, 0.0f),
		g(0.0f, 0.0f, 1.0f, 0.0f),
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Takes an x, y, and z displacement and outputs a 4x4 translation matrix
gMatrix4 gMatrix4::translation3D(float x, float y, float z) {
	gVector4
		e(1.0f, 0.0f, 0.0f, x),
		f(0.0f, 1.0f, 0.0f, y),
		g(0.0f, 0.0f, 1.0f, z),
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Takes an x, y, and z scale and outputs a 4x4 scale matrix
gMatrix4 gMatrix4::scale3D(float x, float y, float z) {
	gVector4
		e(x, 0.0f, 0.0f, 0.0f),
		f(0.0f, y, 0.0f, 0.0f),
		g(0.0f, 0.0f, z, 0.0f),
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Generates a 4x4 identity matrix
gMatrix4 gMatrix4::identity() {
	gVector4
		e(1.0f, 0.0f, 0.0f, 0.0f),
		f(0.0f, 1.0f, 0.0f, 0.0f),
		g(0.0f, 0.0f, 1.0f, 0.0f),
		h(0.0f, 0.0f, 0.0f, 1.0f);
	gMatrix4 trans(e, f, g, h);

	return trans;
}

/// Checks if m1 == m2
bool gMatrix4::operator==(const gMatrix4& m2) {
	for (int i = 0; i < 4; i++)
	{
		if (data[i] != m2[i]) {
			return false;
		}
	}
	return true;
}

/// Checks if m1 != m2
bool gMatrix4::operator!=(const gMatrix4& m2) {
	if (!(*this == m2)) {
		return true;
	}
	else {
		return false;
	}
}

/// Matrix addition (m1 + m2)
gMatrix4 gMatrix4::operator+(const gMatrix4& m2) {
	gMatrix4 add;
	for (int i = 0; i < 4; i++)
	{
		add[i] = data[i] + m2[i];
	}
	return add;
}

/// Matrix subtraction (m1 - m2)
gMatrix4 gMatrix4::operator-(const gMatrix4& m2) {
	gMatrix4 sub;
	for (int i = 0; i < 4; i++)
	{
		sub[i] = data[i] - m2[i];
	}
	return sub;
}

/// Scalar multiplication (m * c)
gMatrix4 gMatrix4::operator*(float c) {
	gMatrix4 multi;
	for (int i = 0; i < 4; i++)
	{
		multi[i] = data[i] * c;
	}
	return multi;
}

/// Scalar multiplication (c * m)
gMatrix4 operator*(float c, const gMatrix4& m) {
	gMatrix4 multi;
	for (int i = 0; i < 4; i++)
	{
		multi[i] = m[i] * c;
	}
	return multi;
}

/// Matrix multiplication (m1 * m2)
gMatrix4 gMatrix4::operator*(const gMatrix4& m2) {
	gMatrix4 multi;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			multi[i][j] = data[i] * m2.getColumn(j);
		}
	}
	return multi;
}

/// Assume v is a column vector (ie. a 4x1 matrix)
gVector4 gMatrix4::operator*(const gVector4& v) {
	gVector4 vec;
	for (int i = 0; i < 4; i++)
	{
		vec[i] = data[i] * v;
	}
	return vec;
}